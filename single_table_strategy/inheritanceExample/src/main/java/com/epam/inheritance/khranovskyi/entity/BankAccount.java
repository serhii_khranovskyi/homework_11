package com.epam.inheritance.khranovskyi.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DiscriminatorValue("BA")
@ToString
@EqualsAndHashCode
public class BankAccount extends BillingDetails{

    @Column(name = "account")
    private String account;

    @Column(name = "bank_name")
    private String bankName;
}
