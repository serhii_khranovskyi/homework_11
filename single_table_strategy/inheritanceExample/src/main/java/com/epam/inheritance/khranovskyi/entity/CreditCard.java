package com.epam.inheritance.khranovskyi.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DiscriminatorValue("CC")
@ToString
@EqualsAndHashCode
public class CreditCard extends BillingDetails {

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "year")
    private Integer expYear;

    @Column(name = "month")
    private Integer expMonth;
}
