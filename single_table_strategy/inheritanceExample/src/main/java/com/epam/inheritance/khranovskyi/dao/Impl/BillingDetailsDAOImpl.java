package com.epam.inheritance.khranovskyi.dao.Impl;

import com.epam.inheritance.khranovskyi.dao.BillingDetailsDAO;
import com.epam.inheritance.khranovskyi.entity.BillingDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class BillingDetailsDAOImpl implements BillingDetailsDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<BillingDetails> get(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from BillingDetails").getResultList();
    }
}
