package com.epam.inheritance.khranovskyi;

import com.epam.inheritance.khranovskyi.config.HibernateConfig;
import com.epam.inheritance.khranovskyi.dao.Impl.BillingDetailsDAOImpl;
import com.epam.inheritance.khranovskyi.entity.BankAccount;
import com.epam.inheritance.khranovskyi.entity.BillingDetails;
import com.epam.inheritance.khranovskyi.entity.Buyer;
import com.epam.inheritance.khranovskyi.entity.CreditCard;
import net.bytebuddy.utility.RandomString;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@SpringBootTest
@Configuration
@ContextConfiguration(classes = {HibernateConfig.class})
@ComponentScan(basePackages = "com.epam.inheritance.khranovskyi")
@EnableTransactionManagement
class DemoApplicationTests {
    private static final Logger LOGGER = Logger.getLogger("DemoApplicationTests");

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private BillingDetailsDAOImpl billingDetailsDAO;

    @Test
    void getMethodTest() {
        EntityManager entityManager = sessionFactory.createEntityManager();

        entityManager.getTransaction().begin();
        Buyer buyer = createBuyer();
        entityManager.persist(buyer);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        Buyer secondBuyer = createBuyer();
        entityManager.persist(secondBuyer);
        entityManager.getTransaction().commit();

        Set<Buyer> buyerSet = new HashSet<>();
        buyerSet.add(buyer);
        buyerSet.add(secondBuyer);
        Set<Buyer> buyerHashSet = new HashSet<>();
        buyerSet.add(secondBuyer);

        entityManager.getTransaction().begin();
        CreditCard creditCard = createCreditCard();
        creditCard.setParentBill(buyerSet);
        entityManager.persist(creditCard);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        BankAccount bankAccount = createBankAccount();
        bankAccount.setParentBill(buyerHashSet);
        entityManager.persist(bankAccount);
        entityManager.getTransaction().commit();

        List<BillingDetails> billingDetailsList = billingDetailsDAO.get(1L);

        billingDetailsList.forEach(p -> {
            if (p instanceof BankAccount) {
                Assert.assertEquals(p, bankAccount);
                LOGGER.info(p.toString());
            }
            if (p instanceof CreditCard) {
                Assert.assertEquals(p, creditCard);
                LOGGER.info(p.toString());
            }
        });
    }

    private BankAccount createBankAccount() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccount(RandomString.make(6));
        bankAccount.setBankName(RandomString.make(5));
        return bankAccount;
    }

    private Buyer createBuyer() {
        Buyer buyer = new Buyer();
        buyer.setFirstName(RandomString.make(5));
        buyer.setLastName(RandomString.make(5));
        return buyer;
    }

    private CreditCard createCreditCard() {
        CreditCard creditCard = new CreditCard();
        creditCard.setCardNumber(RandomString.make(10));
        creditCard.setExpYear((int) (1 + Math.random() * 20));
        creditCard.setExpMonth((int) (1 + Math.random() * 12));
        return creditCard;
    }

}
