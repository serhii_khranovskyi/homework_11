package com.epam.inheritance.khranovskyi.entity;

import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "billing_details")
@Inheritance(strategy = InheritanceType.JOINED)
@ToString
@EqualsAndHashCode
public class BillingDetails {

        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;

        @ManyToMany(fetch = FetchType.EAGER)
        @JoinTable(name = "buyer_bill",
                joinColumns = {
                        @JoinColumn(name = "billing_details_id", referencedColumnName = "id")},
                inverseJoinColumns = {
                        @JoinColumn(name = "buyer_id", referencedColumnName = "id")
                })
        private Set<Buyer> parentBill = new HashSet<>();


}
