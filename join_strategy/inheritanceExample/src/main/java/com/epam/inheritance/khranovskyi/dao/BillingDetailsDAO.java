package com.epam.inheritance.khranovskyi.dao;

import com.epam.inheritance.khranovskyi.entity.BillingDetails;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface BillingDetailsDAO {
    List<BillingDetails> get(Long id);
}
